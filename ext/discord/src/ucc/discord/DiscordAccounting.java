package ucc.discord;

import com.iillyyaa2033.discord.v8.DiscordHttp;
import com.iillyyaa2033.discord.v8.objects.User;
import ucc.accounting.Account;
import ucc.accounting.Accounting;
import ucc.accounting.AdapterInfo;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

public class DiscordAccounting extends Accounting {

    @Override
    public int getCurrentDatabaseVersion() {
        return 1;
    }

    @Override
    public void onDatabaseUpdate(Connection connection, int from) throws SQLException {
        Statement stmt = connection.createStatement();

        switch (from) {
            case 0:
                stmt.execute("create table accounts(" +
                        " id integer primary key autoincrement," +
                        " token text," +
                        " username text," +
                        " avatar text)");
                // No break; here
            case 1:
                // Update from 1 to 2
        }

        stmt.close();
    }

    @Override
    public AdapterInfo info() {
        AdapterInfo info = new AdapterInfo();
        info.id = "dc";
        info.name = "Discord";
        return info;
    }

    @Override
    public Account[] listAccounts() {
        ArrayList<Account> accounts = new ArrayList<>();

        try {
            Statement stmt = database().createStatement();
            ResultSet rs = stmt.executeQuery("select id, username, avatar from accounts order by username;");
            while (rs.next()) {
                Account account = new Account();
                account.id = rs.getString(1);
                account.name = rs.getString(2);
                account.avatar = rs.getString(3);
                accounts.add(account);
            }
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return accounts.toArray(new Account[0]);
    }

    @Override
    public void addAccount(String[] data) {
        try {
            String token = data[0];
            DiscordHttp.Settings settings = new DiscordHttp.Settings(token, false);
            settings.userAgent = "Mozilla/5.0 (Android 7.1.2; Mobile; rv:67.0) Gecko/67.0 Firefox/67.0";
            DiscordHttp api = new DiscordHttp(settings);
            User me = api.getCurrentUser();
            System.out.println("I am " + me.username);

            PreparedStatement stmt = database().prepareStatement(
                    "insert into accounts(token, username, avatar) values (?, ?, ?);"
            );
            stmt.setString(1, token);
            stmt.setString(2, me.username);
            stmt.setString(3, getAvatarUrl(me, 512));
            stmt.execute();

        } catch (IOException | SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteAccount(String id) {
        try {
            PreparedStatement stmt = database().prepareStatement("delete from accounts where id = ?;");
            stmt.setString(1, id);
            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String getAvatarUrl(User user, int size) {
        String baseUrl = "https://cdn.discordapp.com/";
        if (user.avatar == null) {
            int disc = Integer.parseInt(user.discriminator) % 5;
            return baseUrl + "embed/avatars/" + disc + ".png?size=" + size;
        } else {
            return baseUrl + "avatars/" + user.id + "/" + user.avatar + ".png?size=" + size;
        }
    }

}
