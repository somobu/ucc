
## Bugs

### Uncaught SyntaxError

This error may be caused by default WebView unable to process modern-flavored javascript.

Possible workaround:
1. Find, download and install newer System WebView (you can use [this](https://play.google.com/store/apps/details?id=com.google.android.webview), for example);
2. Go to Settings -> Developer options -> WebView implementation and choose your newely-installed implementation;
3. Force-stop UCC and open login page again.
