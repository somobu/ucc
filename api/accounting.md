# objects

## AdapterInfo

| Name | Type | Desc                |
| ---  | ---  | ---                 |
| id   | str  | Identifier          |
| name | str  | Human-readable name |

## Account

Account info

| Name      | Type | Desc                   |
| ---       | ---  | ---                    |
| id        | str  | Identifier             |
| name      | str  | Human-readable name    |
| avatar    | str  | User icon              |

# methods

## info

Get adapter info

| Name    | Type        | Desc |
|---------|-------------|------|
| @return | AdapterInfo |      |

## listAccounts

List all available accounts

| Name     | Type      | Desc |
|----------|-----------|------|
| @return  | Account[] |      |

## addAccount

This is final step of user logging-in. Here we're passing account data to our adapter.
Account data is adapter-dependent. For example, it may be `[user, password]` array.

| Name    | Type  | Desc                                    |
|---------|-------|-----------------------------------------|
| data    | str[] | Array of adapter-dependent account data |

## deleteAccount

| Name | Type | Desc       |
|------|------|------------|
| id   | str  | Account id |

# events

## onAccountsListChanged

When account created, changed or removed

| Name     | Type | Desc               |
|----------|------|--------------------|
| adapter  | str  | Adapter identifier |
