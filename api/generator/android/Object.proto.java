package ucc.api;

import android.os.Parcel;
import android.os.Parcelable;

/* Generator: descr */
public class OBJECT_NAME implements Parcelable {
/* Generator: fields */

    public static final Creator<OBJECT_NAME> CREATOR = new Creator<OBJECT_NAME>() {
        @Override
        public OBJECT_NAME createFromParcel(Parcel in) {
            return new OBJECT_NAME(in);
        }

        @Override
        public OBJECT_NAME[] newArray(int size) {
            return new OBJECT_NAME[size];
        }
    };

    public OBJECT_NAME() {

    }

    protected OBJECT_NAME(Parcel in) {
/* Generator: parcel in */
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
/* Generator: parcel out */
    }

    @Override
    public int describeContents() {
        return 0;
    }
}
