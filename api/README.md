# API

So this is UCC API folder.

Unlike some other APIs this one should be generated from API .md descriptions -- `accounting.md` and `adapter.md`.

## How to generate

**Linux**:

- make sure you have `bash` and `python3` installed;
- just run `generate.sh` from this folder.

## What is it? Why it's needed?

The purpose of entire `api/` folder is to provide API between code interacting with certain messaging service and a code
providing user interface.

## Apidoc

Each Markdown apidoc describes a communication between "server" and "client" in a language- and transport-agnostic
manner.

tl;dr Just a Markdown file with first- (`# Header`) and second-level (`## Header`) sections filled with tables.

Each first-level section **must** be named by a keyword (see below). Each second-level section should be named
in `camelCase` or `snake_case` manner in English letters only (`^[_a-zA-Z]+$`) because this name will be used in
code-generated source files. Also, each second-level section **may** contain plain text lines and **must** contain one
and only one table. That table **must** have columns of structure specified by first-level section.

### Methods section

Client can call a methods on server. Each method has unique name and a set of arguments. Each argument has a name
(English letters and underscores only, unique in the scope of a method), a type and a human-readable description (may be
empty). Type may be primitive (like `str`) or complex (user-defined type, analog of C `struct` datatype). Argument
named `@return` describes return value of this method. Methods description placed in an `methods` section of apidoc as
shown in example below:

```markdown
# methods

## getSomeExampleData

Here goes method description

| Name      | Type        | Desc                  |
| ---       | ---         | ---                   |
| firstArg  | str         | Argument description  |
| @return   | ComplexData | Return description    |
```

Example above will be code-generated into this fragment of Java code:

```java
public class Server {

    // Aux.code omitted

    /**
     * Here goes method description
     *
     * @param firstArg Argument description
     * @return Return description
     */
    public ComplexData getSomeExampleData(String firstArg) {
        throw new RuntimeException("Unimplemented");
    }
}
```

Notes:

- method calls are supposed to be *blocking*;
- there are no exceptions mechanism or optional nulls -- each method **must** return a value;
- there are no method overloading -- each method **must** have unique name;

### Events section

Server may notify clients in a broadcast-like manner by calling an event methods on each of the connected clients. These
events are described in an `events` section of apidoc in the same manner as methods.

### Objects section and data types

`objects` section contains description of complex data passed between client and server.

The same way as `methods` and `events`, subsection title holds structure name, and table describes field's names and
types.

Example:

```markdown
# objects

## AdapterInfo

| Name | Type | Desc                |
| ---  | ---  | ---                 |
| id   | str  | Identifier          |
| name | str  | Human-readable name |
```

generated into:

```java
/**
 */
public class AdapterInfo {

    /**
     * Identifier
     */
    public String id = null;

    /**
     * Human-readable name
     */
    public String name = null;

}
```

Built-in data types:

- `bool`: Boolean;
- `str`: String;
- `int` 32-bit signed integer;
- `float` 32-bit float;
- Any datatype can have `[]` suffix making it an array/vector;

## How code generation works

![](https://gitlab.com/iria_somobu/ucc/-/raw/master/doc/res/apigen_scheme.png)

