package com.somobu.ucc

/**
 * A very basic exceptions collector
 *
 * Proper implementation should notify user and suggest creating error report,
 * but even simple implementation is good for now.
 */
object ShitCollector {

    var STACKOVERFLOW_BULLYING_ENABLED = BuildConfig.DEBUG

    fun feed(reason: String?, throwable: Throwable) {
        println(reason)
        throwable.printStackTrace()

        if (STACKOVERFLOW_BULLYING_ENABLED) {
            println("So, you have an error? Don't cry, try this:")

            val query = (throwable.javaClass.simpleName + " " + throwable.message).replace(" ", "+")
            println("https://www.stackoverflow.com/search?q=[java]+${query}")
        }
    }
}
