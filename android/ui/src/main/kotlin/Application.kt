package com.somobu.ucc

import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration

class Application : android.app.Application() {

    override fun onCreate() {
        super.onCreate()
        val loader = ImageLoader.getInstance()
        if (!loader.isInited) {
            val defaultOptions = DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .build()
            val config = ImageLoaderConfiguration.Builder(this)
                    .defaultDisplayImageOptions(defaultOptions)
                    .build()
            loader.init(config)
        }
    }

}