@file:Suppress("FunctionName")

package com.somobu.ucc;

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            LeftPanel()
        }

        // TODO: go find accounts here
//        startActivity(Intent(this, AccountsActivity::class.java))
    }

    private val colors = darkColors(
        surface = Color(0xFF2C2C2C),
        background = Color(0xFF1F1F1F),
        primary = Color(0xFF657349)
    )

    @Preview
    @Composable
    fun LeftPanel() {
        MaterialTheme(colors = colors) {
            Surface {
                Column(
                    Modifier
                        .width(260.dp)
                        .fillMaxHeight()
                ) {
                    ServerHeader()
                    ThickDivider()

                    LazyColumn(Modifier
                        .padding(0.dp)
                        .weight(1f)
                    ) {
                        items(250) { index ->
                            ChannelItem(index)
                        }
                    }

                    ThickDivider()
                    ServersRow()
                    AccountsRow()
                }
            }
        }
    }

    @Composable
    fun ThickDivider() {
        Divider(
            Modifier
                .height(2.dp)
        )
    }

    @Composable
    fun ServerHeader() {
        Row(
            Modifier
                .fillMaxWidth()
                .padding(0.dp)
                .background(MaterialTheme.colors.background)
        ) {
            Text(
                "Server row",
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold,
                modifier = Modifier
                    .padding(12.dp, 6.dp)
                    .weight(1f)
            )
            IconButton(onClick = {}) {
                Icon(painterResource(R.drawable.ic_contacts),
                    null,
                    Modifier
                        .width(42.dp)
                        .height(42.dp)
                        .padding(0.dp)
                )
            }
        }
    }

    @Composable
    fun ServersRow() {
        Row(
            Modifier
                .fillMaxWidth()
                .height(50.dp)
                .padding(0.dp)
                .background(MaterialTheme.colors.background)
        ) {

            LazyRow {
                items(15) { idx ->
                    ServerItem(idx)
                }
            }

        }
    }

    @Composable
    fun AccountsRow() {
        Row(Modifier
            .fillMaxWidth()
            .height(48.dp)
            .padding(0.dp)
            .background(MaterialTheme.colors.background)
        ) {

            Row(verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.weight(1f)) {

                Image(
                    painterResource(R.mipmap.ic_launcher),
                    null
                )

                Column(
                    Modifier
                        .weight(1f)
                        .padding(start = 8.dp)
                ) {
                    Text("Account name", fontSize = 18.sp)
                    Text("Adapter name", fontSize = 12.sp)
                }
            }

            IconButton(onClick = {}) {
                Icon(painterResource(R.drawable.ic_action_overflow),
                    null,
                    Modifier
                        .width(32.dp)
                        .height(32.dp)
                        .padding(0.dp)
                )
            }
        }
    }


    @Composable
    fun ServerItem(idx: Int) {
        Image(
            painterResource(R.mipmap.ic_launcher),
            null,
            Modifier
                .width(50.dp)
                .height(50.dp)
                .padding(2.dp)
                .background(if (idx == 2) MaterialTheme.colors.primary else Color.Unspecified)
                .clickable {  }
        )
    }

    @Composable
    fun ChannelItem(idx: Int) {
        Row(Modifier
            .fillMaxWidth()
            .padding(0.dp)
            .background(if (idx == 2) MaterialTheme.colors.primary else Color.Unspecified)
            .clickable {  }
        ) {
            Text("Channel " + idx.toString().hashCode().toString(),
                fontSize = 14.sp,
                modifier = Modifier
                    .padding(8.dp, 4.dp)
                    .weight(1f)
            )
        }
    }
}